from setuptools import setup

setup(
    name='Flask-etcd',
    version='1.0',
    license='BSD',
    author='Miles Wilson',
    author_email='miles@presentthinking.net',
    description='Provides a flask client to python-etcd',
    py_modules=['flask_etcd'],
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=[
        'Flask',
        'python-etcd'
    ],
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
    
    