import docker
import sys
c = docker.Client(base_url='unix:///docker.sock',
                  version='1.9',
                  timeout=10)
port = c.port(sys.argv[1], sys.argv[2])
print port[0]['HostPort']
