#!/bin/python
import socket
import sys
import os
import serf
import time
import re
import json
import requests

_client = serf.Client('127.0.0.1:7373', auto_reconnect=False, )
_client.connect()

port = sys.argv[2]
docker_host = sys.argv[1]
host = os.environ.get('SLAVE_PORT_%s_TCP_ADDR' % port)
vulcan_port = os.environ.get('VULCAN_PORT')
vulcan_group = os.environ.get('VULCAN_GROUP')
slave_name = os.environ.get('SLAVE_NAME')
count = 0

def checkConnection(pHost,pPort):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = sock.connect_ex((pHost,int(pPort)))
    if result == 0:
        return True
    else:
        return False

def startMonitor(pHost,pPort):
    while checkConnection(pHost,pPort):
        time.sleep(5)

    print "Port is closed, leaving cluster"
    _client.connect().leave().request()
    print "Left Cluster, We're done here"
    exit(0)

while count < 3:
    portOpen = checkConnection(host,port)
    if portOpen:
        print "Port is open, adding to cluster"
        _responses = _client.join(
            Existing=(docker_host, ),
            Replay=False,
        ).request()

        r = requests.get('http://172.17.42.1:5000/container%s/ports/tag' % slave_name)
        ports = json.loads(r.text)
        tags = {}
        for portMapping in ports:
            portvars = portMapping.split(':')
            tags['port-'+portvars[0]] = "%s:%s" % (portvars[1],portvars[2])

        if vulcan_port:
            tags['vulcan_port'] = vulcan_port
            tags['vulcan_group'] = vulcan_group

        _responses = _client.tags(
            Tags=tags,
            ).request()
        print "Added to cluster, moving to monitor."
        startMonitor(host,port)
        break
    print "Couldn't connect to port, trying again in 5 seconds - %s" % count
    time.sleep(5)
    count = count +1

print "Timed out after three attempts to connect"
