#!/usr/bin/python
#vulcan-web

import etcd as etcd
import requests
import json
import crane.common
import os
import sys
import re

for line in sys.stdin:
    member_details = line

member_details = member_details.split()
if not member_details[2] == 'web':
    print 'Quitting because not a web backend'
    exit(0)

tags = {}
for tag in member_details[3].split(','):
    tag_data = re.search('(\w+)\=(\w+)',tag)
    tags[tag_data.group(1)] = tag_data.group(2)

if not 'vulcan_port' in tags:
    print 'No Vulcan Port tag, dropping'
    exit(0)

member = dict(hostname=member_details[0],ip=member_details[1])

print str(member_details)

tagged_ports = json.loads(requests.get('http://localhost:5000/container/etcd-host/ports/tag').text)
ports = common.decomposeTaggedPorts(tagged_ports)

client = etcd.Client(host=ports['4001'][0], port=int(ports['4001'][1]))

client.write('/vulcand/upstreams/%s/endpoints/%s' % (tags['vulcan_group'],member['hostname']), 'http://%s:%s/' % (member['ip'],tags['vulcan_port']))
client.write('/vulcand/hosts/%s.delfiassociates.dev/locations/app/path' % tags['vulcan_group'] , "/.*")
client.write('/vulcand/hosts/%s.delfiassociates.dev/locations/app/upstream'% tags['vulcan_group'],  tags['vulcan_group'])
