### SideKick Library
import config
import common

class SideKickController:


    docker = 0
    hostip = 0
    etcd = 0

    def __init__(self,dockerMgmtInstance, hostip):
        self.docker = dockerMgmtInstance
        self.hostip = hostip


    def launchContainer(self,
        containerName,
        monitorPort,
        role,
        command = "python -m SimpleHTTPServer 80",
        additionalPorts=[],
        name='web',
        truePortMap=False,
        expose=False,
        vulcan_group=False,
        vulcan_port=False):
        serf_port = common.PickUnusedPort()
        app_port = common.PickUnusedPort()
        app_ports = [monitorPort]
        port_bindings = {}
        for port in additionalPorts:
            app_ports.append(port)
            if truePortMap:
                port_bindings[port] = port
            else:
                port_bindings[port] = common.PickUnusedPort()
        if truePortMap:
            port_bindings[monitorPort] = monitorPort
        else:
            port_bindings[monitorPort] = common.PickUnusedPort()
        app_container = self.docker.create_container(containerName,
            name="%s-%s" % (name,app_port),
            command=command,
            ports=app_ports,
            environment=['ROLE=%s' % role])

        sidekick_env = ['SERF_PORT=%s' % serf_port,'HOST_IP=%s' % self.hostip]
        if vulcan_port:
            sidekick_env.append('VULCAN_GROUP='+vulcan_group)
            sidekick_env.append('VULCAN_PORT='+str(port_bindings[vulcan_port]))

        sidekick_container = self.docker.create_container(config.SIDEKICK_IMAGE_NAME,
            command='/launch.sh %s %s' % (monitorPort, 'Hello'),
            ports=[7946,(7946, 'udp'),7373],
            environment=sidekick_env)

        self.docker.start(app_container, port_bindings=port_bindings)
        self.docker.start(sidekick_container,
            port_bindings={'7946/udp': serf_port, 7946: serf_port},
            links=[("%s-%s" % (name,app_port),'slave')])

