import socket
from flask import Blueprint, render_template, abort, current_app, request
from jinja2 import TemplateNotFound
import etcd as etcd
from crane.app_description import CraneAppDescription, CraneContainer
import crane.common as common
import json
import pprint
import sidekick
import requests
from crane.common import async


name = 'launch'
b_launch = Blueprint('launch','launch')



@b_launch.route('/provision/<app_name>', methods=['GET', 'PUT'])
def provision_app(app_name):
    if request.method == 'PUT':
        current_app.etcd_client.write('/apps/%s/definition' % app_name, str(request.get_data()))
        app_definition = current_app.etcd_client.get('/apps/%s/definition' % app_name)
    if request.method == 'GET':
        app_definition = current_app.etcd_client.get('/apps/%s/definition' % app_name)
    blob = json.loads(app_definition.value)

    return pprint.pformat(blob)


@b_launch.route('/_launch_machine/<app_name>/<container_name>')
def _launch_machine(app_name,container_name):
    sk = sidekick.SideKickController(current_app.docker_client, current_app.config['HOST_UP'])
    app_definition = current_app.etcd_client.get('/apps/%s/definition' % app_name)
    blob = json.loads(app_definition.value)
    app = CraneAppDescription().from_json(blob)
    containers = dict(app.containers)
    pprint.pprint(containers)
    container = containers[container_name]
    sk.launchContainer(containerName=container.repository,
                       monitorPort=container.monitor_port,
                       role=container.role,
                       command=container.command,
                       additionalPorts=container.ports,
                       vulcan_port=container.vulcan_port,
                       vulcan_group=app.endpoint,
                       name=container.name)
    current_app.etcd_client.write('/apps/%s/%s/hosts/%s' % (app_name, container_name, socket.gethostname()), 'active')
    return str(True)
@async
def _call_launch(host_ip, app_name, container_name):
    url = 'http://%s:5000/launch/_launch_machine/%s/%s' % (host_ip ,app_name, container_name)
    requests.get(url)

@b_launch.route('/cluster/<app_name>')
def launch_cluster(app_name):
    #Determine if there is a **not** currently running the image.
    app = _get_app(app_name)
    for container in app.containers:
        host = _viable_host(app_name,container[0])
        host_ip = current_app.etcd_client.read('/hosts/%s' % host, recursive=True, sorted=True).value
        output =  'Launching %s on %s' % (container[0], host_ip)
        _call_launch(host_ip,app_name,container[0])

    return output


def _get_app(app_name):
    app_definition = current_app.etcd_client.get('/apps/%s/definition' % app_name)
    blob = json.loads(app_definition.value)
    app = CraneAppDescription().from_json(blob)
    assert isinstance(app, CraneAppDescription)
    return app

def _viable_host(app_name, container_name):
    try:
        current_app.etcd_client.read('/apps/%s/%s' % (app_name, container_name))
        app_known = True
    except KeyError:
        print 'No instances of container running'
        app_known = False
    not_viable_hosts = []
    if app_known is not False:
        hosts_running_app = current_app.etcd_client.read('/apps/%s/%s/hosts' % (app_name, container_name), recursive=True, sorted=True)
        pprint.pprint(hosts_running_app)
        for host in hosts_running_app.children:
            not_viable_hosts.append(host.key)
    possible_hosts = []
    hosts_total = current_app.etcd_client.read('/hosts')
    pprint.pprint(hosts_total)
    for host in hosts_total.children:
        possible_hosts.append(host.key.split('/')[-1])
    viable_hosts = list(set(possible_hosts) - set(not_viable_hosts))
    if not len(viable_hosts) > 0:
        current_app.logger.debug('All hosts are running the current app, grab the next most free one')
        viable_hosts = [current_app.getFreeHost()]
    return viable_hosts[0]



    