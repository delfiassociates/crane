__author__ = 'mileswilson'

class CraneAppDescription:

    def __init__(self):
        self.containers = []
        self.name = 'default_app'
        self.endpoint = 'dummy.delfiassociates.dev'

    def from_json(self, json_blob):
        """
        :type self.containers list(CraneContainer)
        """
        self.containers = []
        self.endpoint = json_blob['endpoint']
        for json_container in json_blob['containers']:
            self.containers.append((json_container['name'], CraneContainer(name=json_container['name'], repository=json_container['repository'],ports=json_container['ports'],
                                                  monitor_port=json_container['monitor_port'],vulcan_port=json_container['vulcan_port'],
                                                  scale=json_container['scale'],command=json_container['command'])))
        self.name = json_blob['name']

        assert isinstance(self, CraneAppDescription)
        return self



class CraneContainer:

    def __init__(self, repository='ubuntu', ports=[], monitor_port=False, vulcan_port=False, scale=False, role="web", command="", name=""):
        """

        :type self: CraneContainer
        """
        self.repository = repository
        self.ports = ports
        self.monitor_port = monitor_port
        self.vulcan_port = vulcan_port
        self.scale = scale
        self.command = command
        self.role = role
        self.name = name

