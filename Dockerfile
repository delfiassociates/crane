FROM ubuntu
RUN apt-get update;apt-get install -y python-pip wget unzip
RUN pip install docker-py serf-python flask
EXPOSE 7001
EXPOSE 4001
EXPOSE 5000
